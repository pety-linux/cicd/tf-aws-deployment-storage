# Intro
This CICD Pipeline provides the Terraform code for Deployment of Resources on AWS.
It can be used e.g. for Deployment of Your K8S cluster.

## Structure of repo
- terraform folder contains Terraform code
- .gitlab-ci.yml contains logis for deployment

## Scope of AWS Resources
- vpc
- igw
- rt 
- ec2 
- sg 

## How to use it
In order to use this Pipeline you just need to provide the valid AWS credentials (e.g. taken from aCloudguru).
```
Settings -> CI/CD -> Variables 
```
```
AWS_DEFAULT_REGION
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```
## Limits
- allowed regions: us-east-1 and us-east-2
- max 10 EC2s
- max 10 vCPU across all instances
- allowed instance sizes:
  - t2.micro to t2.medium
  - t3.micro to t3.medium
- os_disk max 50GB


## UseCase

rook-ceph deployment

`https://rook.opensovereigncloud.com`
