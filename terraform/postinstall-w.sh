#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

exec > /tmp/debug.$$ 2>&1
set -x

# Variables
LBIP=`hostname -I | awk '{print $1}'`
##

# Install packages
apt update && apt -y install git lvm2
##

# Create User
useradd -s /bin/bash -c "Admin" -m tux
echo "Passw0rd" | passwd --stdin tux
echo "tux ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
##

# Deploy SSH keys
mkdir /home/tux/.ssh
cat <<EOF | tee -a /home/tux/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
EOF
# Set proper permissions
chown -R tux /home/tux/.ssh
chmod 700 /home/tux/.ssh
chmod 600 /home/tux/.ssh/authorized_keys
##


## Join to the Cluster
sleep 3m
masterip=10.0.1.69
wget http://${masterip}:8888/token 
#mastertoken=`cat token`
#k3s agent --server https://${masterip}:6443 --token ${mastertoken}
##


# Join Cluster as Worker
export INSTALL_K3S_VERSION="v1.32.0+k3s1"
export INSTALL_K3S_SKIP_SELINUX_RPM=true
export INSTALL_K3S_SELINUX_WARN=true
export K3S_URL=https://${masterip}:6443
export K3S_TOKEN=`cat token`

# Install
curl -sfL https://get.k3s.io | sh -

# Start & Enable agent
systemctl enable --now k3s-agent
