#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

exec > /tmp/debug.$$ 2>&1
set -x

# Variables
LBIP=`hostname -I | awk '{print $1}'`

# Install packages
#apt update && apt -y install git jq nginx
apt update && apt -y install git jq nginx ceph-common lvm2

# hostname
hostnamectl set-hostname master --static

# Create User
useradd -s /bin/bash -c "Admin" -m tux
echo "Passw0rd" | passwd --stdin tux

# Set sudo
echo "tux ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Deploy SSH keys
mkdir /home/tux/.ssh
cat <<EOF | tee -a /home/tux/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOFAcEobnJc+o0jEBN1t/rBKU+scMHPtG4Keail7rJhs micuper
EOF
# Set proper permissions
chown -R tux /home/tux/.ssh
chmod 700 /home/tux/.ssh
chmod 600 /home/tux/.ssh/authorized_keys
##

## nginx
apt -y install nginx
sed -i 's/listen 80/listen 8888/g' /etc/nginx/sites-available/default
systemctl restart nginx 
#

## Init K3S
# Variables
export INSTALL_K3S_VERSION="v1.32.0+k3s1"
# Install
#curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--disable=traefik" sh -
curl -sfL https://get.k3s.io | sh -s server - --cluster-init --disable traefik --etcd-snapshot-schedule-cron "*/30 * * * *" --etcd-snapshot-retention "10" --kube-apiserver-arg="enable-bootstrap-token-auth=true" 


# kubeconfig
mkdir -p /root/.kube
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
cp -i /etc/rancher/k3s/k3s.yaml /root/.kube/config
chmod +r /etc/rancher/k3s/k3s.yaml
echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> /etc/bashrc
cp /var/lib/rancher/k3s/server/token /var/www/html/token
chmod 644 /var/www/html/token
#

## Ingress Controller deployment
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.2/deploy/static/provider/baremetal/deploy.yaml
sed -i "s/NodePort/LoadBalancer/g" deploy.yaml
sed -i "/LoadBalancer/a\  externalIPs:\n    - $LBIP" deploy.yaml
kubectl apply -f deploy.yaml
##

## Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
##

## Install kubens
wget https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz
tar xvzf kubens_v0.9.4_linux_x86_64.tar.gz
mv kubens /usr/local/bin/
##

## Metrics Server
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
##

## cert-manager
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.8.0/cert-manager.yaml
##

## Rook/ceph
rook_ver=v1.16.0

# rook operator
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/crds.yaml
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/common.yaml
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/operator.yaml
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/csi/rbd/storageclass.yaml
sleep 3
# change default storageclass
kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
kubectl patch storageclass rook-ceph-block -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'

# form clusterrook-ceph.example.com
wget https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/cluster.yaml
sed -i "/network:/a\    hostNetwork: true" cluster.yaml
sed -i "/# encryptedDevice: /a\      encryptedDevice: \"true\"" cluster.yaml
#kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/cluster.yaml
kubectl apply -f cluster.yaml
sleep 5

# install toolbox
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/toolbox.yaml

# expose dashboard
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/dashboard-external-https.yaml
wget https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/dashboard-ingress-https.yaml
sed -i 's/rook-ceph.example.com/rook.opensovereigncloud.com/g' dashboard-ingress-https.yaml  
kubectl apply -f dashboard-ingress-https.yaml
##

# kustomize
snap install kustomize

# go
wget https://go.dev/dl/go1.20.1.linux-amd64.tar.gz
tar -C /usr/local -xvzf go1.20.1.linux-amd64.tar.gz
export GO111MODULE=on
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/go
export GOCACHE=$HOME/go/cache
source /etc/bashrc


## rook TLS
cat <<EOF | tee -a rook.crt
-----BEGIN CERTIFICATE-----
MIIFAjCCA+qgAwIBAgISBJTNuWZMRWlkMGTmtsroG0hbMA0GCSqGSIb3DQEBCwUA
MDIxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MQswCQYDVQQD
EwJSMzAeFw0yNDAxMjUxMDU5NTJaFw0yNDA0MjQxMDU5NTFaMCYxJDAiBgNVBAMT
G3Jvb2sub3BlbnNvdmVyZWlnbmNsb3VkLmNvbTCCASIwDQYJKoZIhvcNAQEBBQAD
ggEPADCCAQoCggEBAMm57htsjt/RTKfOh6l8OAwSzN9kNXoixyKtUyJkwQ5Fnr98
Fm3HachBNoQZ+MzVINt4bLpcMiRhP5cilqSQ1pCGsOwmufkO3PDp/UGrj3cHF7HG
g0vMwvf49ary/XD/6PsvgQIFgl+Lmn2Abxdfr9gGqqI63x+q7IE19hgTq4ZJKHBv
3WQ/OfacBDjHNuIYupDuMYCpkEetpDgDCs4dibDSGK7yHgZwtyeKYcSlfQ00wCmB
tNU+cq9opNNXyaJFh0W+NxEim5Vo0L49/RU3utbhPCCT5T5qcSaudEKG4eAkRblY
ybVNQKE3/sKliZ+l0Vqar+6Duox5LRTyadEatsMCAwEAAaOCAhwwggIYMA4GA1Ud
DwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwDAYDVR0T
AQH/BAIwADAdBgNVHQ4EFgQUX/AXOGM41SndJlYJdDko5RiHz7YwHwYDVR0jBBgw
FoAUFC6zF7dYVsuuUAlA5h+vnYsUwsYwVQYIKwYBBQUHAQEESTBHMCEGCCsGAQUF
BzABhhVodHRwOi8vcjMuby5sZW5jci5vcmcwIgYIKwYBBQUHMAKGFmh0dHA6Ly9y
My5pLmxlbmNyLm9yZy8wJgYDVR0RBB8wHYIbcm9vay5vcGVuc292ZXJlaWduY2xv
dWQuY29tMBMGA1UdIAQMMAowCAYGZ4EMAQIBMIIBAwYKKwYBBAHWeQIEAgSB9ASB
8QDvAHYAO1N3dT4tuYBOizBbBv5AO2fYT8P0x70ADS1yb+H61BcAAAGNQH2ljQAA
BAMARzBFAiAA/5vFxrbLXR3pR9zsSSkCcY6pui3qowb+ZosCmgjj+wIhAOVOFYnE
Em17vb1/BVy/sp5z6I4Hzwuiwsokxcz/0N4gAHUAouK/1h7eLy8HoNZObTen3GVD
sMa1LqLat4r4mm31F9gAAAGNQH2lnQAABAMARjBEAiAIrebAZ1O3YqWF4o2FSZ60
yubMb7mJLPxT5jgIi8mOywIgDbRYU9isaKVeQlhFRZxjXwwpM/2bVw3UTseNmR6Q
dYYwDQYJKoZIhvcNAQELBQADggEBAFNyiI5jUIiEuOTM7YLl6tRKgsOVQcZJQAZI
Ll8WsUwrfjVZP7LcGn7XsD///3/74i/Du8+6tG1StQ1OkajWUjtFUoZfYtiuNay/
qtiuanRGrAHtwJV6qFPnZMnVeFIqFYOTfNPWeEZuBhhhhwgQnZ7oRL5B+d6SmH8Q
n/8mUqFSKkauXYUBJY7Aut+NrjnwtdWztui3xQHymanDWrA4GjoWm3XbXquyZdCA
erW8qCvF06KvYOrkaZ6QVdDamCeZHLySlgaWhpN9ELtdO4LhE0tf/12gfOG7JdXY
hRmSxO9i9bNUVvcorikiuvjxe1vDFsn7Wjp808dBsI6XB7TEmDo=
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIFFjCCAv6gAwIBAgIRAJErCErPDBinU/bWLiWnX1owDQYJKoZIhvcNAQELBQAw
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMjAwOTA0MDAwMDAw
WhcNMjUwOTE1MTYwMDAwWjAyMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNTGV0J3Mg
RW5jcnlwdDELMAkGA1UEAxMCUjMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
AoIBAQC7AhUozPaglNMPEuyNVZLD+ILxmaZ6QoinXSaqtSu5xUyxr45r+XXIo9cP
R5QUVTVXjJ6oojkZ9YI8QqlObvU7wy7bjcCwXPNZOOftz2nwWgsbvsCUJCWH+jdx
sxPnHKzhm+/b5DtFUkWWqcFTzjTIUu61ru2P3mBw4qVUq7ZtDpelQDRrK9O8Zutm
NHz6a4uPVymZ+DAXXbpyb/uBxa3Shlg9F8fnCbvxK/eG3MHacV3URuPMrSXBiLxg
Z3Vms/EY96Jc5lP/Ooi2R6X/ExjqmAl3P51T+c8B5fWmcBcUr2Ok/5mzk53cU6cG
/kiFHaFpriV1uxPMUgP17VGhi9sVAgMBAAGjggEIMIIBBDAOBgNVHQ8BAf8EBAMC
AYYwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMBMBIGA1UdEwEB/wQIMAYB
Af8CAQAwHQYDVR0OBBYEFBQusxe3WFbLrlAJQOYfr52LFMLGMB8GA1UdIwQYMBaA
FHm0WeZ7tuXkAXOACIjIGlj26ZtuMDIGCCsGAQUFBwEBBCYwJDAiBggrBgEFBQcw
AoYWaHR0cDovL3gxLmkubGVuY3Iub3JnLzAnBgNVHR8EIDAeMBygGqAYhhZodHRw
Oi8veDEuYy5sZW5jci5vcmcvMCIGA1UdIAQbMBkwCAYGZ4EMAQIBMA0GCysGAQQB
gt8TAQEBMA0GCSqGSIb3DQEBCwUAA4ICAQCFyk5HPqP3hUSFvNVneLKYY611TR6W
PTNlclQtgaDqw+34IL9fzLdwALduO/ZelN7kIJ+m74uyA+eitRY8kc607TkC53wl
ikfmZW4/RvTZ8M6UK+5UzhK8jCdLuMGYL6KvzXGRSgi3yLgjewQtCPkIVz6D2QQz
CkcheAmCJ8MqyJu5zlzyZMjAvnnAT45tRAxekrsu94sQ4egdRCnbWSDtY7kh+BIm
lJNXoB1lBMEKIq4QDUOXoRgffuDghje1WrG9ML+Hbisq/yFOGwXD9RiX8F6sw6W4
avAuvDszue5L3sz85K+EC4Y/wFVDNvZo4TYXao6Z0f+lQKc0t8DQYzk1OXVu8rp2
yJMC6alLbBfODALZvYH7n7do1AZls4I9d1P4jnkDrQoxB3UqQ9hVl3LEKQ73xF1O
yK5GhDDX8oVfGKF5u+decIsH4YaTw7mP3GFxJSqv3+0lUFJoi5Lc5da149p90Ids
hCExroL1+7mryIkXPeFM5TgO9r0rvZaBFOvV2z0gp35Z0+L4WPlbuEjN/lxPFin+
HlUjr8gRsI3qfJOQFy/9rKIJR0Y/8Omwt/8oTWgy1mdeHmmjk7j1nYsvC9JSQ6Zv
MldlTTKB3zhThV1+XWYp6rjd5JW1zbVWEkLNxE7GJThEUG3szgBVGP7pSWTUTsqX
nLRbwHOoq7hHwg==
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
DkRTVCBSb290IENBIFgzMB4XDTIxMDEyMDE5MTQwM1oXDTI0MDkzMDE4MTQwM1ow
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwggIiMA0GCSqGSIb3DQEB
AQUAA4ICDwAwggIKAoICAQCt6CRz9BQ385ueK1coHIe+3LffOJCMbjzmV6B493XC
ov71am72AE8o295ohmxEk7axY/0UEmu/H9LqMZshftEzPLpI9d1537O4/xLxIZpL
wYqGcWlKZmZsj348cL+tKSIG8+TA5oCu4kuPt5l+lAOf00eXfJlII1PoOK5PCm+D
LtFJV4yAdLbaL9A4jXsDcCEbdfIwPPqPrt3aY6vrFk/CjhFLfs8L6P+1dy70sntK
4EwSJQxwjQMpoOFTJOwT2e4ZvxCzSow/iaNhUd6shweU9GNx7C7ib1uYgeGJXDR5
bHbvO5BieebbpJovJsXQEOEO3tkQjhb7t/eo98flAgeYjzYIlefiN5YNNnWe+w5y
sR2bvAP5SQXYgd0FtCrWQemsAXaVCg/Y39W9Eh81LygXbNKYwagJZHduRze6zqxZ
Xmidf3LWicUGQSk+WT7dJvUkyRGnWqNMQB9GoZm1pzpRboY7nn1ypxIFeFntPlF4
FQsDj43QLwWyPntKHEtzBRL8xurgUBN8Q5N0s8p0544fAQjQMNRbcTa0B7rBMDBc
SLeCO5imfWCKoqMpgsy6vYMEG6KDA0Gh1gXxG8K28Kh8hjtGqEgqiNx2mna/H2ql
PRmP6zjzZN7IKw0KKP/32+IVQtQi0Cdd4Xn+GOdwiK1O5tmLOsbdJ1Fu/7xk9TND
TwIDAQABo4IBRjCCAUIwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYw
SwYIKwYBBQUHAQEEPzA9MDsGCCsGAQUFBzAChi9odHRwOi8vYXBwcy5pZGVudHJ1
c3QuY29tL3Jvb3RzL2RzdHJvb3RjYXgzLnA3YzAfBgNVHSMEGDAWgBTEp7Gkeyxx
+tvhS5B1/8QVYIWJEDBUBgNVHSAETTBLMAgGBmeBDAECATA/BgsrBgEEAYLfEwEB
ATAwMC4GCCsGAQUFBwIBFiJodHRwOi8vY3BzLnJvb3QteDEubGV0c2VuY3J5cHQu
b3JnMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly9jcmwuaWRlbnRydXN0LmNvbS9E
U1RST09UQ0FYM0NSTC5jcmwwHQYDVR0OBBYEFHm0WeZ7tuXkAXOACIjIGlj26Ztu
MA0GCSqGSIb3DQEBCwUAA4IBAQAKcwBslm7/DlLQrt2M51oGrS+o44+/yQoDFVDC
5WxCu2+b9LRPwkSICHXM6webFGJueN7sJ7o5XPWioW5WlHAQU7G75K/QosMrAdSW
9MUgNTP52GE24HGNtLi1qoJFlcDyqSMo59ahy2cI2qBDLKobkx/J3vWraV0T9VuG
WCLKTVXkcGdtwlfFRjlBz4pYg1htmf5X6DYO8A4jqv2Il9DjXA6USbW1FzXSLr9O
he8Y4IWS6wY7bCkjCWDcRQJMEhg76fsO3txE+FiYruq9RUWhiF1myv4Q6W+CyBFC
Dfvp7OOGAN6dEOM4+qR9sdjoSYKEBpsr6GtPAQw4dy753ec5
-----END CERTIFICATE-----
EOF

cat <<EOF | tee -a rook.key
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDJue4bbI7f0Uyn
zoepfDgMEszfZDV6IscirVMiZMEORZ6/fBZtx2nIQTaEGfjM1SDbeGy6XDIkYT+X
IpakkNaQhrDsJrn5Dtzw6f1Bq493BxexxoNLzML3+PWq8v1w/+j7L4ECBYJfi5p9
gG8XX6/YBqqiOt8fquyBNfYYE6uGSShwb91kPzn2nAQ4xzbiGLqQ7jGAqZBHraQ4
AwrOHYmw0hiu8h4GcLcnimHEpX0NNMApgbTVPnKvaKTTV8miRYdFvjcRIpuVaNC+
Pf0VN7rW4Twgk+U+anEmrnRChuHgJEW5WMm1TUChN/7CpYmfpdFamq/ug7qMeS0U
8mnRGrbDAgMBAAECggEATnWkqMH2aBMt726i0UbhPcDqMlD1g8WXEmO7m/r4I6bs
cam/x37yI9NJcuPvIywaFYKnN2y4i/HPG+d2FBfJfHkGwzLkznv7bzBzMmGZ8T+u
9ZQyaP8BLhWE57KO5K6EXX5CfRjLdoID2qBjlQHAkRM2ttaTBewjBLaKOx8bbaTB
8QnQxEqlz4iwHXrLzqPkfJewJFnIcFDUsqZXiDClLH30aJqu9gcKUWatjg0J74Dl
FCPNemzFS8zePwYcMGNlyqXA3x4xCbH9kz4i7PvgpSjAwvmlhJsiR4VpZqPEFbKh
ZdBBfurRFYwQ0nNZlwP/oei7kkSlEP17/m5H4LXVYQKBgQDkGlnZGR8v33h+gzwU
4CTwaEGYoDEd9+PIkfX0yitCbrs/OLhE0soHcIBHoCIB6vDEk/NwF7S7P5JxHlO/
JyR2ljI8UYrPRsFwghZnEzpVL7Q4GPERDCX38X98EmnMVSowyGvKl7YqP/7jxWYd
nwdWhuPaWSF8MTvqEkCzPBu8mwKBgQDiZcF5t4luZks8iHXtyfRmqqGAjUs5bR8x
zdTnG8l9TIr6vT19CeREfWbfBXRGmksB0hZG2N93we2Fj1SOBW1znD+YmROrNMW2
rI99ob+OGLCKq8wfE1kdAUiXIWDtrUwVTFePtIP8PmwKZIzGGx32CY/aHOyLZBym
3EQt8esM+QKBgEIKFwUxrsPPcTU/jX+FvWwU4xjrGA1THZGOIKV0er3LDU8qfr7+
QBuTSLBz/iSL/pULOKm1U0/7AczaOOjYzke/CGBKQ2PrWknWf+HrwTG0k/rBzdlf
qQZ11rD3K8ZKyeJBxGNtNQJacNNE7WGaDyfP+dvXfkhUgMIt8qlxt54NAoGBAIyg
MCFuw8rzd2BOUxI1E76B1neHTGaLRYkmFUzIkctmtB0zVvJIJqX0TuXQ+Xyxck3b
NUMkiwpURzli4l6lOGtLbqVW0ETULstdPHQI+HdHFsHMHKpg1kWR+okFjrOTpWLF
XM5CjoksLOf3tQmI5n9hSR1uQfADa1iqER+I2TvhAoGBAJR2ajPTR9ckvKJIMeda
nvFlIdh0HL8+yce8cgZaDAZGR7yazWeRqfibgRcxdnofWKo2ACY7LWUh7Gm79rXb
reerNKbmYsYfnRk+e5YVZ3ej5DNwdXykC7z6qA7GIf6t7l8rSrbBiydSBLcORYHZ
nT6+8kOzwCVZgvMxqfpyDMFP
-----END PRIVATE KEY-----
EOF
#

# rook TLS secret
kubectl create secret tls rook.opensovereigncloud.com --namespace rook-ceph --key rook.key --cert rook.crt
##

# ingress resource
sleep 3m
kubectl apply -f https://gitlab.com/pety-linux/cicd/tf-aws-deployment-storage/-/raw/main/terraform/ingress.yaml

## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias s='sudo su -'
alias k='k3s kubectl'
alias c=clear
export GO111MODULE=on
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/go
export GOCACHE=$HOME/go/cache
EOF
source /etc/bashrc

cat <<EOF | tee -a /home/tux/.bashrc
# Aliases
alias s='sudo su -'
alias k='k3s kubectl'
alias c=clear
export GO111MODULE=on
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/go
export GOCACHE=$HOME/go/cache
EOF
chown tux:tux /home/tux/.bashrc 
source /home/tux/.bashrc
##

## Motd
unlink /etc/motd
cat <<EOF | tee -a /etc/motd
┌─────────────────────────────────────────────────────────────┐
│┌───────────────────────────────────────────────────────────┐│
││                 Welcome to OSC Environment!               ││
││───────────────────────────────────────────────────────────││
││                                                           ││
││         This is your K3S CEPH multi-node cluster.         ││
││                                                           ││
│└───────────────────────────────────────────────────────────┘│
└─────────────────────────────────────────────────────────────┘
EOF
###
